<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Producto;

class ControladorProducto extends Controller
{
    //
    public function index(){
      $productos = Producto::all();
      /*
      return $productos;
      return "LA PAGINA DE PRODUCTO";
      */

      return view('productos.index')->with('lista_productos',$productos);
    }

    public function create(){
      return view('productos.crear');
    }

    public function store(Request $request){
      /*
      $product = new Producto;
      $product->nombre= $request->nombre;
      $product->precio= $request->precio;
      $product->save();*/

      $inputs = $request->all();
      $producto=Producto::Create($inputs);
      return redirect()->route('producto.index');

    }

    public function show($id){
      $product = Producto::find($id);
      //return $product;
      return view('productos.show')->with('producto',$product);
    }

    public function destroy ($id){
      Producto::destroy($id);
      $productos = Producto::all();
      return redirect()->route('producto.index');
    }


    public function edit($id){
      $product = Producto::find($id);
      return view('productos.edit')->with('producto',$product);
    }

    public function update(Request $request, $id){
      $product = Producto::find($id);
      $product->nombre=$request->nombre;
      $product->precio=$request->precio;

      $product->save();

      return redirect()->route('producto.index');
    }
}
