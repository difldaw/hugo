<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;

class ControladorPaginas extends Controller
{
  public function getAcerca(){
    //Le vamos a pasar como variable el nombre de la compañia
    $nombre_comp = "HCDESARROLLO";
    $estaregistrado = false;

    $lista_nombres = array("Hugo", "Carla", "Armando", "Alex");
    return view('paginas.acerca')
      ->with("nombre", $nombre_comp)
      ->with("estaregistrado", $estaregistrado)
      ->with("usuarios", $lista_nombres);
  }

  public function getContacto(){
    return view('paginas.contacto');
  }

  public function getAyuda(){
    return view('paginas.ayuda');
  }

  public function getIndex(){
    return view('welcome');
  }
}
