<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|


Route::get('/', function () {
    return view('welcome');
});*/

//Esta es la primera prueba de ruta que se hace en el tutorial 3, solo regresa un string
Route::get('regresa_string', function () {
    return "Hello World";
});

//Esta es la segunda prueba que regresa una vista
Route::get('regresa_vista', function () {
    return view('paginas.prueba1');
});

/*
La parte numero 3 del tutorial pide que creemos
3 rutas diferentes para 3 vistas diferentes
*/
/*
Route::get('acerca', function () {
    return view('paginas.acerca');
});
Route::get('contacto', function () {
    return view('paginas.contacto');
});
Route::get('ayuda', function () {
    return view('paginas.ayuda');
});

*/

/*
La parte numero 4 del tutorial pide que creemos
rutas "compuestas"
*/
Route::get('primero/segundo', function () {
    return view('paginas.prueba1');
});



/* Tutorial #4

  Nos pide que utilicemos los named routes

  */

  Route::get('acerca', [
      'as' => 'acerca',
      'uses' => 'ControladorPaginas@getAcerca'
  ]);

  Route::get('contacto', [
      'as' => 'contacto',
      'uses' => 'ControladorPaginas@getContacto'
  ]);

  Route::get('ayuda', [
      'as' => 'ayuda',
      'uses' => 'ControladorPaginas@getAyuda'
  ]);

  Route::get('/', [
      'as' => '/',
      'uses' => 'ControladorPaginas@getIndex'
  ]);


  Route::resource('producto', 'ControladorProducto');

//PARTE 1 de TUTORIAL

/*
  Route::get('basicrouting', function(){
    return "This is basic routing.";
  });
*/
Route::get('basicrouting', function(){
  return view('welcome');
});


Route::match(['get','post','delete'],'matchrouting',function(){
  return 'matchrouting';
  //return view('welcome');
});
Route::any('anyroute',function(){
  return 'anyroute';
});

Route::group(['as'=>'admin::'],function(){
  Route::get('dashboard', [
    'as' => 'dashboard',
    'uses' => 'PagesController@getIndex'
  ]);
  Route::get('reports', [
    'as' => 'namedRoutes',
    'uses' => 'PagesController@getIndex'
  ]);
});

Route::get('user/{id}',function($id){
  //$user = User::find($id);

  return $id;
  //return view('welcome');
})
->where('id','[0-9]+');
Route::get('namedRoutes', [
  'as' => 'namedRoutes',
  'uses' => 'PagesController@getIndex'
]);
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
