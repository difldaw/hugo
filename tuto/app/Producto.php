<?php

/*
  RECUERDA QUE EN LA BASE DE DATOS SE ESCRIBE EL NOMBRE DEL "OBJETO" EN MINUSCULAS Y EN PLURAL

  EN ESTE CASO LA TABLA DE Producto SE LLAMA productos

*/

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    //ESTA LINEA ES LA QUE DICE LA TABLA QUE SE VA A UTILIZAR EN CASO DE NO CUMPLIR CON LO ANTERIOR
    protected $table='productos';
    public $timestamps = false;
    protected $fillable = ['nombre','precio'];
}
