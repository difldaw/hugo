
@extends('layouts.layout_gen')


@section('title')
  Acerca de Nosotros
@stop

@section('body')
  <h1>Esta es la Pagina de ACERCA DE NOSOTROS </h1>

  <p>Mi compañia es : {{$nombre}}</p>
  @if($estaregistrado==true)
    <p>Hola compi</p>
  @else
    <p>Porfa ingresa a la pagina</p>
  @endif
  @for ($i = 0; $i < 10; $i++)
    <p>The current value is {{ $i }}</p>
  @endfor

  <h3>Nuestro equipo es : </h3>
  <ul>
    @foreach ($usuarios as $usuario)
      <li>{{ $usuario }}</li>
    @endforeach
  </ul>
@stop
