<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title> @yield('title') </title>
  </head>

  <body>
    @include('layouts.menu')
    @yield('body')
  </body>

</html>
