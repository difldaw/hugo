@extends('layouts.layout_gen')

@section('title')

{{$producto->nombre}}

@stop


@section('body')

  <h1>{{$producto->nombre}}</h1>
  <h3>{{$producto->precio}}</h3>
  <form method="POST" action="{{route('producto.destroy', $producto->id)}}">
    <a href="{{route('producto.edit', $producto->id)}}">edit</a>
    <input type="submit" value="delete">
    {{ method_field('DELETE') }}
  </form>
@stop
