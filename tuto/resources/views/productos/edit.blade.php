@extends('layouts.layout_gen')

@section('title')

Editar {{$producto->nombre}}

@stop


@section('body')
  <h1> Edita Producto {{$producto->nombre}}</h1>
  <form method="POST" action="{{route('producto.update',$producto->id)}}">
    <input type="text" name="nombre" value="{{$producto->nombre}}">
    <input type="text" name="precio" value="{{$producto->precio}}">
    <input type="submit" value="Editar">
    {{ method_field('PATCH') }}
  </form>

@stop
